<?php wp_footer(); ?>
<script src="https://cdn.jsdelivr.net/npm/pjax-api@latest"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/common.js"></script>
<script>
let changeBg = $('#change_bg');
$('.bg_change').hover(
	function() {
		changeBg.addClass('js_active');
		$(this).addClass('js_active');
		hoverBg($(this).attr('data-id'));
		$('.mainContent').addClass('js_active');
	},
	function(){
		changeBg.removeClass('js_active');
		$(this).removeClass('js_active');
		hoverBg(false);
		$('.mainContent').removeClass('js_active');
	}
)

function hoverBg(num){
	if( num !== false ){
		$('.change_bg__item').eq(num).addClass('active');
	}else{
		$('.change_bg__item').removeClass('active');
	}
}

function add_class_in_scrolling(target) {
    var winScroll = $(window).scrollTop();
    var winHeight = $(window).height();
	var scrollPos = winScroll + winHeight;
	if(target){
		if(target.offset().top < scrollPos) {
			target.addClass('animationActive');
		}
	}else{
		console.log('なし');
	}
	// if(target != true){
	// 	if(target.offset().top < scrollPos) {
	// 		target.addClass('animationActive');
	// 	}
	// }else{
	// 	alert('なし')
	// }
}
// const btn = document.querySelector("#btn-mode");

// // チェックした時の挙動
// btn.addEventListener("change", () => {
//   if (btn.checked == true) {
//     // ダークモード
//     document.body.classList.remove("light-theme");
//     document.body.classList.add("dark-theme");
//   } else {
//     // ライトモード
//     document.body.classList.remove("dark-theme");
//     document.body.classList.add("light-theme");
//   }
// });
</script>
<script>
add_class_in_scrolling($('.animationLeft'));
</script>
</body>
</html>