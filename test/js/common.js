// PJAX
var Pjax = require('pjax-api').Pjax;
new Pjax({
  areas: [
	'#mainWrap , #sideBar, #mainContent'
  ]
});
window.addEventListener('pjax:fetch', function () {
	// alert('s');
});
// クリック後
$(window).on('pjax:unload', function(){
	console.log("pjax:unload")
	// alert('unload');

});
$(window).on('pjax:content', function(){
	console.log("pjax:content")
	// alert('content');
});
// 更新後
$(document).on('pjax:ready', function(){
	console.log("pjax:ready")
	// alert('render2');
});

// すべての画像(IMG要素)とフレーム(IFRAME, FRAME要素)の読み込み後、windowオブジェクトから発生します。
$(window).on('pjax:load', function(){
	console.log("pjax:load")
	// alert('load');
});

