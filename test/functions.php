<?php
remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');
remove_action('wp_head', 'wp_generator');
function my_delete_plugin_files() {
    wp_dequeue_style('wp-pagenavi');
}
add_action( 'wp_enqueue_scripts', 'my_delete_plugin_files' );
add_action( 'wp_footer', function() {
    wp_deregister_script( 'wp-embed' );
});
function dequeue_plugins_style() {
    wp_dequeue_style('wp-block-library');
}
add_action( 'wp_enqueue_scripts', 'dequeue_plugins_style', 9999);
remove_action('wp_head', 'wp_shortlink_wp_head');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'rsd_link');
remove_action('wp_head','rest_output_link_wp_head');
remove_action('wp_head','wp_resource_hints',2);