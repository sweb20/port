<!DOCTYPE html>
<!-- <html lang="ja"> -->
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Iam Seiya Nakagawa</title>
    <!-- meta -->
    <!-- css -->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/style.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/index.css">
    <!-- js -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>

    <!-- pjax -->
    
    <!-- <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.pjax.js"></script> -->
    <!-- icon -->
    <script src="https://unpkg.com/ionicons@5.2.3/dist/ionicons.js"></script>
    <!-- font -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Noto+Sans+JP:wght@400;500;700&family=Rubik:wght@300;400&display=swap" rel="stylesheet">
</head>
<body>
<?php wp_head(); ?>
<header id="header" class="header">
        <ul class="header__nav">
            <li class="header__item">
                <a href="">ABOUT</a>
            </li>
            <li class="header__item">
                <a class="button_icon">
                    <div class="svgSize"><svg xmlns="http://www.w3.org/2000/svg" class="ionicon s-ion-icon" viewBox="0 0 512 512"><title>Contrast</title><path d="M256 32A224 224 0 0097.61 414.39 224 224 0 10414.39 97.61 222.53 222.53 0 00256 32zM64 256c0-105.87 86.13-192 192-192v384c-105.87 0-192-86.13-192-192z"></path></svg></div>
                </a>
            </li>
        </ul>
    </header>