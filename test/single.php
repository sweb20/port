<?php get_header(); ?>

<!-- contens start -->
<main id="mainWrap" class="mainWrap">
    <?php get_sidebar(); ?>
    <div id="mainContent" class="mainContent">
    <?php if ( have_posts() ) : ?>
  <?php while ( have_posts() ) : the_post(); ?>
      <h2><?php the_title(); ?></h2>
  <?php endwhile;?>
<?php endif; ?>
    </div>
</main>
<!-- contens end -->

<?php get_footer(); ?>