<?php get_header(); ?>
<?php
    $args = array(
        'post_type' => 'post',      // 投稿
        'posts_per_page' => 10,     // 表示する投稿数(-1を指定すると全投稿を表示)
        'order' => 'ASC'           // ソートの並び順を指定'DESC' 降順、'ASC' 昇順
        );
    $st_query = new WP_Query( $args );
    $targetCnt = 0;
?>
<!-- contens start -->
<main id="mainWrap" class="mainWrap">
    <div id="change_bg" class="change_bg">
    <?php if ( $st_query->have_posts() ): ?>
    <?php while ( $st_query->have_posts() ) : $st_query->the_post(); ?>
        <?php $cf_bg = get_field('cf_bg'); ?>
        <div class="change_bg__item" style="background-image:url('<?php if($cf_bg){echo $cf_bg;} ?>')"></div>
    <?php endwhile; ?>
    <?php else: ?>
        <p>error</p>
    <?php endif; ?>
    </div>
    <?php get_sidebar(); ?>
    <div id="mainContent" class="mainContent">
        <?php if ( $st_query->have_posts() ): ?>
            <?php while ( $st_query->have_posts() ) : $st_query->the_post(); ?>
            <?php
                $cf_date = get_field('cf_date'); 
                $cf_num = get_field('cf_num'); 
            ?>
            <article class="articleItem animation">
                <h2 class="articleItem__title bg_change" data-id="<?php echo $targetCnt ?>">
                    <a href="<?php the_permalink(); ?>">
                        <span class="articleItem__num">
                            <?php if($cf_num){echo $cf_num;} ?>
                        </span>
                        <div class="animation--cmb animationWeight animationLeft"><?php the_title(); ?></div>
                    </a>
                </h2>
                <div class="articleItem__flex animation--cmb animationLeft">
                    <div class="articleItem__date">
                        <p>
                            <?php if($cf_date){echo $cf_date;} ?>
                        </p>
                    </div>
                    <div class="articleItem__category">
                        <p>
                        <?php
                            $str = '';
                            foreach((get_the_category()) as $cat){
                                $str .= $cat->cat_name . ' , ';
                            }
                            echo rtrim($str, " , ");
                        ?>
                        </p>
                    </div>
                </div>
            </article>
        <?php $targetCnt++; endwhile; ?>
        <?php else: ?>
            <p>error</p>
        <?php endif; ?>
        </div>
    </main>
<!-- contens end -->

<?php get_footer(); ?>
